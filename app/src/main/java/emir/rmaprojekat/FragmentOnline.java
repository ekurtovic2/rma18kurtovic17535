package emir.rmaprojekat;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.Console;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentOnline extends Fragment implements DohvatiKnjige.IDohvatiKnjigeDone,View.OnClickListener,
        DohvatiNajnovije.IDohvatiNajnovijeDone,MojResultReceiver.Receiever{
    private Spinner spinerKategorija;
    private Spinner spinnerKnjiga;
    private ArrayList<String> kategorije;
    private ArrayAdapter<String> adapter;
    private ArrayAdapter<String> adapterOnline;
    private ArrayList<Knjiga> rezultat;
    private ArrayList<String> rezultatString;

    private Button pretraga,povratak,dodaj;
    private EditText tekstUpit;



    public FragmentOnline() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_online, container, false);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        /*Dohvatiti reference*/
        spinerKategorija=(Spinner) getActivity().findViewById(R.id.sKategorije);
        spinnerKnjiga=(Spinner) getActivity().findViewById(R.id.sRezultat);
        pretraga=(Button) getActivity().findViewById(R.id.dRun);
        povratak=(Button) getActivity().findViewById(R.id.dPovratak);
        dodaj=(Button) getActivity().findViewById(R.id.dAdd);
        tekstUpit=(EditText) getActivity().findViewById(R.id.tekstUpit);
        /*Dohvacanje pod iz Bundle-a*/
        rezultatString=new ArrayList<String>();
        kategorije=getArguments().getStringArrayList("KEY2");
        adapter=new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1,kategorije);
        spinerKategorija.setAdapter(adapter);
        adapterOnline=new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1,rezultatString);
        spinnerKnjiga.setAdapter(adapterOnline);

        /*Postaviti listenere*/
        pretraga.setOnClickListener(this);
        povratak.setOnClickListener(this);
        dodaj.setOnClickListener(this);
    }

    @Override
    public void onDohvatiDone(ArrayList<Knjiga> knjige) {
        rezultat=new ArrayList<Knjiga>(knjige);
        rezultatString=new ArrayList<String>();
        for(Knjiga x:rezultat){
            rezultatString.add(x.getNaziv());
        }
        if(getContext()!=null) {
            adapterOnline = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, rezultatString);
            spinnerKnjiga.setAdapter(adapterOnline);
        }
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.dRun){
            String upit=new String(tekstUpit.getText().toString());
            if(upit.startsWith("autor:")){
                String upit2=upit.replace("autor:","");
                new DohvatiNajnovije((DohvatiNajnovije.IDohvatiNajnovijeDone)FragmentOnline.this).execute(upit2);
            }
            else if(upit.startsWith("korisnik:")){
                String upit2=upit.replace("korisnik:","");
                Intent intent=new Intent(Intent.ACTION_SYNC,null,getActivity(),KnjigePoznanika.class);

                MojResultReceiver mReceiever=new MojResultReceiver(new Handler());
                mReceiever.setReceiver(FragmentOnline.this);
                //ovdje valjda fragment implementira interfejs, a ne aktivnost;posto je on receiver

                intent.putExtra("idKorisnika",upit2);
                intent.putExtra("receiver",mReceiever);

                getActivity().startService(intent);
            }
            else if(!upit.isEmpty()){
                String[] array = upit.split(";");
                for (String x : array) {
                    Log.d("poruka", x);
                }
                new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone) FragmentOnline.this).execute(array);
            }
        }
        else if(view.getId()==R.id.dPovratak){
            ((KategorijeAkt)getActivity()).otvoriListe();
        }
        else if(view.getId()==R.id.dAdd){
            if(!adapterOnline.isEmpty()&& !adapter.isEmpty()){
                int position=spinnerKnjiga.getSelectedItemPosition();
                Knjiga k=rezultat.get(position);
                k.dopuniOnlineUFull(spinerKategorija.getSelectedItem().toString());
                //baza
                BazaOpenHelper bazaOpenHelper=new BazaOpenHelper(getContext());
                int postoji=bazaOpenHelper.postojiKnjiga(k);
                if(postoji==1){
                    Toast.makeText(getActivity(), "Knjiga vec postoji",
                            Toast.LENGTH_SHORT).show();
                    bazaOpenHelper.close();
                    return;
                }
                else if(postoji==-1){
                    Toast.makeText(getActivity(), "Doslo je do greske",
                            Toast.LENGTH_SHORT).show();
                    bazaOpenHelper.close();
                    return;
                }
                long idNoveKnjige=bazaOpenHelper.dodajKnjigu(k);
                if(idNoveKnjige==-1){
                    Toast.makeText(getActivity(), "Doslo je do greske",
                            Toast.LENGTH_SHORT).show();
                    bazaOpenHelper.close();
                }
                else {
                    bazaOpenHelper.close();
                    ((KategorijeAkt) getActivity()).otvoriListe();
                }
            }
        }
    }

    @Override
    public void onNajnovijeDone(ArrayList<Knjiga> knjige) {
        rezultat=new ArrayList<Knjiga>(knjige);
        rezultatString=new ArrayList<String>();
        for(Knjiga x:rezultat){
            rezultatString.add(x.getNaziv());
        }
        if(getContext()!=null) {
            adapterOnline = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, rezultatString);
            spinnerKnjiga.setAdapter(adapterOnline);
        }
    }

    @Override
    public void onReceiveResult(int ResultCode, Bundle resultData) {
        switch(ResultCode){
            case KnjigePoznanika.STATUS_START:
                Log.d("poruka","poceo");
                break;
            case KnjigePoznanika.STATUS_FINISH:
                Log.d("poruka","zavrsio");
                ZaPrenos zaPrenos=(ZaPrenos)resultData.getSerializable("result");
                ArrayList<Knjiga> rezultat=zaPrenos.dajKnige();
                onNajnovijeDone(rezultat);
                break;
            case  KnjigePoznanika.STATUS_ERROR:
                String error=resultData.getString(Intent.EXTRA_TEXT);
                if(getContext()!=null) {
                    Toast.makeText(getContext(), error, Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
}
