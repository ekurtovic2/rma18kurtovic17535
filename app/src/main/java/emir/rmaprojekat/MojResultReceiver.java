package emir.rmaprojekat;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.os.ResultReceiver;

/**
 * Created by user on 25.05.2018..
 */

public class MojResultReceiver extends ResultReceiver {
    private Receiever mReceiver;

    public MojResultReceiver(Handler handler) {
        super(handler);
    }
    public void setReceiver(Receiever receiver){
        mReceiver=receiver;
    }
    //deklaracija interfejsa kojeg ce trebat implementirati
    public interface Receiever{
        public void onReceiveResult(int ResultCode, Bundle resultData);
    }
    @Override
    protected  void onReceiveResult(int ResultCode, Bundle resultData){
        if(mReceiver!=null){
            mReceiver.onReceiveResult(ResultCode,resultData);
        }
    }
    //dakle posaljemo instancu ove klase intentservisu, onda joj sa send posaljemo result a iz nje proslijedimo receieveru
}
