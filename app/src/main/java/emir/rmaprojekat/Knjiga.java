package emir.rmaprojekat;

import android.media.Image;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.widget.ImageView;

import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by user on 28.03.2018..
 */

public class Knjiga implements Serializable{
    //--------------atributi----------------------
    private String id="",naziv,opis="",datumObjavljivanja="",imePrvogAutora,kategorija;
    private ArrayList<Autor> autori;
    private int brojStranica=-1;
    private boolean kliknuta,online;
    private URL slika=null;
    //obicni-ne posjeduju: id,autori,opis,datumObjavljivanja,slika,brojStranica;
    //full(dobijeni iz online)-ne posjeduju: naslovnaStr;

    //--------------konstruktori-----------------
    //za obicni
    public Knjiga(String imeAutora,String nazivKnjige,String kategorija){
        this.imePrvogAutora=imeAutora;this.naziv=nazivKnjige;this.kategorija=kategorija;
        autori=new ArrayList<Autor>();
        Autor autor=new Autor(imeAutora);
        autori.add(autor);
        kliknuta=false;online=false;
        //Log.d("poruka: ","----------------"+Integer.toString(indeks));
    }
    //za online
    public Knjiga(String id, String naziv, ArrayList<Autor> autori, String opis, String datumObjavljivanja, URL slika, int brojStranica) {
        this.id = id;
        this.naziv = naziv;
        this.opis = opis;
        this.datumObjavljivanja = datumObjavljivanja;
        this.autori = autori;
        this.brojStranica = brojStranica;
        this.slika = slika;
        kliknuta=false;online=true;
    }

    //--------------metode-----------------------
    public void  kliknuto(){kliknuta=true;}
    public void dopuniOnlineUFull(String kategorija){
        setKategorija(kategorija);
        if(!autori.isEmpty()) {
            setImePrvogAutora(autori.get(0).getImeiPrezime());
        }
        else{setImePrvogAutora(null);}
    }
    public void postaviOffline(){online=false;}

    //--------------geteri i seteri--------------
    //samo sa geterom
    public boolean getKliknuta(){return kliknuta;}
    public boolean getOnline(){return online;}
    //i geter i seter
    public String getImePrvogAutora(){return  imePrvogAutora;}
    public void setImePrvogAutora(String imePrvogAutora){this.imePrvogAutora=imePrvogAutora;}
    public String getKategorija(){return kategorija;}
    public void setKategorija(String kategorija){this.kategorija=kategorija;}
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getNaziv(){return naziv;}
    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }
    public String getOpis() {
        return opis;
    }
    public void setOpis(String opis) {
        this.opis = opis;
    }
    public String getDatumObjavljivanja() {
        return datumObjavljivanja;
    }
    public void setDatumObjavljivanja(String datumObjavljivanja) {
        this.datumObjavljivanja = datumObjavljivanja;
    }
    public ArrayList<Autor> getAutori() {
        return autori;
    }
    public void setAutori(ArrayList<Autor> autori) {
        this.autori = autori;
    }
    public int getBrojStranica() {
        return brojStranica;
    }
    public void setBrojStranica(int brojStranica) {
        this.brojStranica = brojStranica;
    }
    public URL getSlika() {
        return slika;
    }
    public void setSlika(URL slika) {
        this.slika = slika;
    }
}
