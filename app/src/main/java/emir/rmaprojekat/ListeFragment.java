package emir.rmaprojekat;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by user on 11.04.2018..
 */

public class ListeFragment extends Fragment implements View.OnClickListener{
    private ArrayList<String> kategorije;
    private ArrayList<Pair<Integer,Autor>> autoriSaId;
    private ArrayList<String> autoriBrKnjiga;

    private ArrayAdapter<String> adapterKategorije;
    private ArrayAdapter<String> adapterAutori;
    private String dozvoljeni;

    private ListView lista;
    private Button pretraga;//pretraga je sad ref na dugme
    private EditText tekst;
    private Button dodajKat;
    private Button dodajKnj;
    private Button noviKategorije;
    private Button noviAutori;
    private Button dodajOnline;

    private boolean kat;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return inflater.inflate(R.layout.fragment_liste,container,false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        kat=false;
        autoriBrKnjiga=new ArrayList<String>();
        autoriSaId=new ArrayList<Pair<Integer, Autor>>();
        /*Dohvacanje pod iz Bundle-a*/
        kategorije=getArguments().getStringArrayList(KategorijeAkt.SVE_KATEGORIJE);//ovo je referenca na kategorije iz poc aktivnosti
        /*Dohvacanje referenci*/
        lista = (ListView) getActivity().findViewById(R.id.listaKategorija);
        pretraga = (Button) getActivity().findViewById(R.id.dPretraga);//pretraga je sad ref na dugme
        tekst = (EditText) getActivity().findViewById(R.id.tekstPretraga);
        dodajKat = (Button) getActivity().findViewById(R.id.dDodajKategoriju);
        dodajKnj=(Button) getActivity().findViewById(R.id.dDodajKnjigu);
        noviKategorije=(Button) getActivity().findViewById(R.id.dKategorije);
        noviKategorije.setEnabled(false);
        noviAutori=(Button) getActivity().findViewById(R.id.dAutori);
        dodajOnline=(Button) getActivity().findViewById(R.id.dDodajOnline);
        /*Dozvoljeni,autori i dva adaptera*/
        //vjer i ne trebaju dva adaptera al et
        dozvoljeni="";

        //moze dosta efikasnije al neda mi se sada
        BazaOpenHelper bazaOpenHelper=new BazaOpenHelper(getContext());
        boolean uspio=bazaOpenHelper.ucitajAutoreSaIdUlistu(autoriSaId);
        if(!uspio){
            Toast.makeText(getActivity(), "Doslo je do greske",
                    Toast.LENGTH_SHORT).show();
            bazaOpenHelper.close();
        }
        else {
            for (Pair<Integer, Autor> a : autoriSaId) {
                Integer brKnjiga = bazaOpenHelper.dajBrKnjigaAutora(a.first);
                if (brKnjiga.equals(-1)) {
                    Toast.makeText(getActivity(), "Doslo je do greske",
                            Toast.LENGTH_SHORT).show();
                    bazaOpenHelper.close();
                    break;
                }
                String b = a.second.getImeiPrezime() + " , br knjiga: " + brKnjiga.toString();
                autoriBrKnjiga.add(b);
            }
            bazaOpenHelper.close();
        }

        adapterKategorije=new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, kategorije);
        adapterAutori=new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1,autoriBrKnjiga);
        /*Event listeneri*/
        tekst.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(!dozvoljeni.equals(tekst.getText().toString())){
                    dodajKat.setEnabled(false);
                    return;
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        pretraga.setOnClickListener(this);
        dodajKat.setOnClickListener(this);
        dodajKnj.setOnClickListener(this);
        noviKategorije.setOnClickListener(this);
        noviAutori.setOnClickListener(this);
        dodajOnline.setOnClickListener(this);
        /*Oko liste*/
        //stavicemo da su po defaultu kategorije posto nije specificirano niti je receno da ne smije biti nista po defaultu ucitano
        lista.setAdapter(adapterKategorije);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(kat==false){
                    //saljemo knjige kategorije
                    String kat=adapterKategorije.getItem(i);
                    //baza
                    BazaOpenHelper bazaOpenHelper=new BazaOpenHelper(getContext());
                    long idKategorije=bazaOpenHelper.dajIdKategorijeIliAutora(BazaOpenHelper.IZBOR_KATEGORIJA,kat);
                    if(idKategorije==-1){
                        Toast.makeText(getActivity(), "Doslo je do greske",
                                Toast.LENGTH_SHORT).show();
                        bazaOpenHelper.close();
                        return;
                    }
                    try {
                        ArrayList<Knjiga> odabraneKnjige = bazaOpenHelper.knjigeKategorije(idKategorije);
                        ((KategorijeAkt)getActivity()).otvoriKnjige(odabraneKnjige);
                        //sad nismo poslali referencu na listu iz pocetne aktivnosti
                    }
                    catch (Exception e){
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Doslo je do greske",
                                Toast.LENGTH_SHORT).show();
                    }
                    bazaOpenHelper.close();
                }
                else{
                    //saljemo knjige autora
                    long idAutora=autoriSaId.get(i).first;//buduci sa nema pretraga isti ce indeksi biti pa mozemo ovako
                    //baza
                    BazaOpenHelper bazaOpenHelper=new BazaOpenHelper(getContext());
                    try {
                        ArrayList<Knjiga> odabraneKnjige = bazaOpenHelper.knjigeAutora(idAutora);
                        ((KategorijeAkt)getActivity()).otvoriKnjige(odabraneKnjige);
                    }
                    catch (Exception e){
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Doslo je do greske",
                                Toast.LENGTH_SHORT).show();
                    }
                    bazaOpenHelper.close();
                }

            }
        });
    }
    @Override
    public void onClick(View view){
        if(view.getId()==R.id.dPretraga){
            if(!tekst.getText().toString().isEmpty()){
                String x=tekst.getText().toString();
                adapterKategorije.getFilter().filter(x);
                //iz nekog razloga ne moze odmah getcount pa cemo probat drukcije-zato stoi je asinhrono pa ne zavrsi?
                int y;
                if(kategorije.contains(x)) y=1;
                else y=0;
                //vratice 0 i ako postoji string koji sadrzi ovaj kao podstring, ali nema veze jer i u tom slucaju treba omoguciti da se doda ovaj zasebno
                if(y==0){
                    dodajKat.setEnabled(true);
                    dozvoljeni=x;
                }
            }
        }
        else if(view.getId()==R.id.dDodajKategoriju){
            if(!dozvoljeni.equals(tekst.getText().toString())){
                dodajKat.setEnabled(false);
                return;//prebacit ovo kasnije u text changed event
            }
            //baza
            BazaOpenHelper bazaOpenHelper=new BazaOpenHelper(getContext());
            long idNoveKategorije=bazaOpenHelper.dodajKategoriju(tekst.getText().toString());
            if(idNoveKategorije==-1){
                Toast.makeText(getActivity(), "Doslo je do greske",
                        Toast.LENGTH_SHORT).show();
            }
            bazaOpenHelper.close();
            //baza
            //kategorije.add(tekst.getText().toString());
            adapterKategorije.notifyDataSetChanged();
            adapterKategorije.clear();
            adapterKategorije=new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, kategorije);
            lista.setAdapter(adapterKategorije);
            tekst.setText("");
            dodajKat.setEnabled(false);
        }
        else if(view.getId()==R.id.dDodajKnjigu){
            ((KategorijeAkt)getActivity()).otvoriDodajKnjigu(kategorije);

        }
        else if(view.getId()==R.id.dAutori){
            noviKategorije.setEnabled(true);
            kat=true;
            noviAutori.setEnabled(false);
            lista.setAdapter(adapterAutori);
            pretraga.setVisibility(View.GONE);
            dodajKat.setVisibility(View.GONE);//nije moglo zbog constrainta
            tekst.setVisibility(View.GONE);
        }
        else if(view.getId()==R.id.dKategorije){
            noviKategorije.setEnabled(false);
            kat=false;
            noviAutori.setEnabled(true);
            lista.setAdapter(adapterKategorije);
            pretraga.setVisibility(View.VISIBLE);
            dodajKat.setVisibility(View.VISIBLE);
            tekst.setVisibility(View.VISIBLE);
        }
        else if(view.getId()==R.id.dDodajOnline){
            ((KategorijeAkt)getActivity()).otvoriDodajOnline(kategorije);
        }
    }
}