package emir.rmaprojekat;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.widget.Toast;

import com.facebook.stetho.Stetho;

import java.util.ArrayList;

public class KategorijeAkt extends AppCompatActivity {
    public static final String SVE_KATEGORIJE="sveKategorije";
    private  ArrayList<String> kategorije;

    private FragmentManager fragmentManager;
    //String msg = "Android log poruka : ";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kategorije_akt);
        Stetho.initializeWithDefaults(this);
        //deleteDatabase(BazaOpenHelper.DATABASE_NAME);
        kategorije=new ArrayList<String>();
        //baza
        //popunjavanje kategorija iz baze
        BazaOpenHelper bazaOpenHelper=new BazaOpenHelper(this);
        boolean uspio=bazaOpenHelper.ucitajKategorijeUlistu(kategorije);
        if(!uspio){
            Toast.makeText(this, "Doslo je do greske",
                    Toast.LENGTH_SHORT).show();
        }
        bazaOpenHelper.close();
        //////////////////////
        /*Dodavanje fragmenta*/
        fragmentManager=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        ListeFragment fragment=new ListeFragment();
        //slanje podataka, mogli smo sve drzat u aktivnosti al bolja praksa slati
        Bundle data=new Bundle();
        data.putStringArrayList(SVE_KATEGORIJE,kategorije);
        fragment.setArguments(data);

        fragmentTransaction.add(R.id.fragment_container,fragment);
        fragmentTransaction.commit();
        //////////////////////
    }
    public void otvoriDodajKnjigu(ArrayList<String> kategorije){
        //this.kategorije=kategorije;
        //mozda updateovat i ovu sto aktivnost drzi kategorije, vidjet kasnije hocel trebat...
        DodavanjeKnjigeFragment dkFragment=new DodavanjeKnjigeFragment();
        Bundle data=new Bundle();
        data.putStringArrayList("KEY",kategorije);
        dkFragment.setArguments(data);
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container,dkFragment);
        fragmentTransaction.commit();
    }
    public void otvoriListe(){
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        ListeFragment fragment=new ListeFragment();
        //slanje podataka, mogli smo sve drzat u aktivnosti al bolja praksa slati
        Bundle data=new Bundle();
        data.putStringArrayList(SVE_KATEGORIJE,kategorije);
        fragment.setArguments(data);

        fragmentTransaction.replace(R.id.fragment_container,fragment);
        fragmentTransaction.commit();
    }
    public void otvoriKnjige(ArrayList<Knjiga> odabrane){
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        KnjigeFragment fragment=new KnjigeFragment();
        Bundle data=new Bundle();
        ZaPrenos prenos=new ZaPrenos(odabrane);
        data.putSerializable("ODABRANE",prenos);
        fragment.setArguments(data);

        fragmentTransaction.replace(R.id.fragment_container,fragment);
        fragmentTransaction.commit();
    }
    public void otvoriDodajOnline(ArrayList<String> kategorije){
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        FragmentOnline fragment=new FragmentOnline();
        Bundle data=new Bundle();
        data.putStringArrayList("KEY2",kategorije);
        fragment.setArguments(data);

        fragmentTransaction.replace(R.id.fragment_container,fragment);
        fragmentTransaction.commit();
    }
    public void otvoriPreporuci(Knjiga k){
        Bundle data=new Bundle();
        data.putSerializable("PREPORUCI",k);

        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        FragmentPreporuci fragment=new FragmentPreporuci();
        fragment.setArguments(data);

        fragmentTransaction.replace(R.id.fragment_container,fragment);
        fragmentTransaction.commit();
    }
    public void dodajKategoriju(String kat){
        if(!kategorije.contains(kat)) kategorije.add(kat);
    }
}
