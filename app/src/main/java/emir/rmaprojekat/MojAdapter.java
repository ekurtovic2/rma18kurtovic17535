package emir.rmaprojekat;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.util.List;

/**
 * Created by user on 30.03.2018..
 */

public class MojAdapter extends ArrayAdapter<Knjiga> {
    int resource;
    //Context context;
    public MojAdapter(Context context, int _resource, List<Knjiga> items){
        super(context,_resource,items);//bazni konstruktor
        resource=_resource;
        //this.context=context;
    }
    //overide metode getView...
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        // Kreiranje i inflate-anje view klase
        LinearLayout newView;
        if (convertView == null) {
            // Ukoliko je ovo prvi put da se pristupa klasi convertView, odnosno nije upadate
            // Potrebno je kreirati novi objekat i inflate-at ga
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)getContext().getSystemService(inflater);
            li.inflate(resource, newView, true);
        }
        else {
            // Ukoliko je update potrebno je samo izmjeniti vrijednosti polja
            newView = (LinearLayout)convertView;
        }
        final Knjiga classInstance = getItem(position);
        // Ovdje možete dohvatiti reference na View i popuniti ga sa vrijednostima polja iz objekta
        ImageView slika=(ImageView)newView.findViewById(R.id.eNaslovna);
        TextView nazivKnjige=(TextView)newView.findViewById(R.id.eNaziv);
        TextView imePisca=(TextView)newView.findViewById(R.id.eAutor);
        TextView datum=(TextView)newView.findViewById(R.id.eDatumObjavljivanja);
        TextView opis=(TextView)newView.findViewById(R.id.eOpis);
        TextView brStranica=(TextView)newView.findViewById(R.id.eBrojStranica);
        Button preporuci=(Button)newView.findViewById(R.id.dPreporuci);
        preporuci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((KategorijeAkt)getContext()).otvoriPreporuci(classInstance);
            }
        });

        nazivKnjige.setText("Naziv: "+classInstance.getNaziv());
        if(classInstance.getImePrvogAutora()!=null) {
            imePisca.setText("Prvi pisac: "+classInstance.getImePrvogAutora());
        }
        else{
            imePisca.setText("Prvi pisac: nema podataka");
        }
        if(!classInstance.getOnline()){
            try {slika.setImageBitmap(BitmapFactory.decodeStream(getContext().openFileInput(classInstance.getNaziv())));}
            catch (FileNotFoundException e) {e.printStackTrace();slika.setImageDrawable(null);}
            Log.d("poruka",classInstance.getNaziv()+" knjiga offline");
        }
        else{
            if(classInstance.getSlika()!=null){
                try{Picasso.get().load(classInstance.getSlika().toString()).into(slika);}
                catch (Exception e){e.printStackTrace();slika.setImageDrawable(null);}
            }
            else {
                slika.setImageDrawable(null);
            }
            Log.d("poruka",classInstance.getNaziv()+" knjiga online");
        }
        if(classInstance.getKliknuta()){
            newView.setBackgroundColor(0xffaabbed);
        }
        if(classInstance.getOnline()){
            if(classInstance.getDatumObjavljivanja()!=null) {
                datum.setText("Datum: " + classInstance.getDatumObjavljivanja());
            }
            else {datum.setText("Datum: nema podataka");}
            if(classInstance.getOpis()!=null){
                opis.setText("Opis: "+classInstance.getOpis());
            }
            else {opis.setText("Opis: nema podataka");}
            Integer br=classInstance.getBrojStranica();
            if(!br.equals(-1)) {
                String broj = Integer.toString(br);
                brStranica.setText("Br stranica: " + broj);
            }
            else{
                brStranica.setText("Br stranica: nema podataka");
            }
        }
        else{
            datum.setText("Datum: nema podataka");
            opis.setText("Opis: nema podataka");
            brStranica.setText("Br stranica: nema podataka");
        }
        return newView;
    }
}
