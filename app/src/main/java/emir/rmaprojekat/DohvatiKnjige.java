package emir.rmaprojekat;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by user on 22.05.2018..
 */

public class DohvatiKnjige extends AsyncTask<String, Integer, Void> {
    //atributi
    private IDohvatiKnjigeDone pozivatelj;
    private ArrayList<Knjiga> rezultat;
    //deklaracija interfejsa
    public interface IDohvatiKnjigeDone{
        public void onDohvatiDone(ArrayList<Knjiga> knjige);
    }
    //konstruktor
    public DohvatiKnjige(IDohvatiKnjigeDone poz){
        pozivatelj=poz;
    }
    //metode
    @Override
    protected Void doInBackground(String... params) {
        rezultat=new ArrayList<Knjiga>();
        for(int i=0;i<params.length;i++) {
            String query = null;
            String urlString=null;
            URL url=null;
            URLConnection connection= null;
            HttpURLConnection httpURLConnection=null;
            int responseCode= 0;
            try {
                query = URLEncoder.encode(params[i],"utf-8");
                urlString="https://www.googleapis.com/books/v1/volumes?q=intitle:"+query+"&maxResults=5";
                url=new URL(urlString);
                connection = url.openConnection();
                httpURLConnection=(HttpURLConnection)connection;
                responseCode = httpURLConnection.getResponseCode();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            if(responseCode==HttpURLConnection.HTTP_OK){
                InputStream in= null;
                String rez=null;
                JSONObject jo = null;//lakse raditi sa JSON
                JSONArray items = null;
                try {
                    in = httpURLConnection.getInputStream();
                    rez=CovertStreamToString(in);//da dobijemo string, kojim cemo inicijalizirati JSON objekat
                    jo = new JSONObject(rez);
                    items = jo.getJSONArray("items");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
                for(int j=0;j<items.length();j++){
                    JSONObject knjiga= null;
                    String id= null;
                    JSONObject knjigaInfo= null;
                    String naziv= null;
                    try {
                        knjiga = items.getJSONObject(j);
                        id = knjiga.getString("id");
                        knjigaInfo = knjiga.getJSONObject("volumeInfo");
                        naziv = knjigaInfo.getString("title");
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                    //ove vr moze da nema
                    JSONArray autori= null;
                    //opis,datumObjavljivanja,br Stranica
                    String opis= null;
                    String datumObjavljivanja= null;
                    int brStranica= -1;
                    ArrayList<Autor> authors=new ArrayList<Autor>();
                    try {
                        autori = knjigaInfo.getJSONArray("authors");
                        for(int k=0;k<autori.length();k++){
                            String autor= null;
                            try {
                                autor = autori.getString(k);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            //provjera da li vec postoji
                            //ako ne postoji, dodati
                            Autor a1=new Autor(autor,id);
                            authors.add(a1);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {opis = knjigaInfo.getString("description");} catch (JSONException e) {e.printStackTrace();}
                    try {datumObjavljivanja = knjigaInfo.getString("publishedDate");} catch (JSONException e) {e.printStackTrace();}
                    try {brStranica = knjigaInfo.getInt("pageCount");} catch (JSONException e) {e.printStackTrace();}
                    //slika
                    JSONObject thumb=null;
                    String url1=null;
                    URL slika=null;
                    try {
                        thumb = knjigaInfo.getJSONObject("imageLinks");
                        url1 = thumb.getString("thumbnail");
                    }
                    catch (JSONException e){
                        e.printStackTrace();
                    }
                    try {slika = new URL(url1);} catch (MalformedURLException e) {e.printStackTrace();}
                    //
                    //treba iznad u try/catch il nest jer neamju svi sve podfatke
                    Knjiga k=new Knjiga(id,naziv,authors,opis,datumObjavljivanja,slika,brStranica);
                    rezultat.add(k);
                }
            }
        }
        return null;
    }

    public static String CovertStreamToString(InputStream is){
        //BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        BufferedReader reader=new BufferedReader(new InputStreamReader(is));
        //StringBuilder sb = ​new StringBuilder();
        StringBuilder sb=new StringBuilder();
        //String line = ​null ​;
        String line=null;
        try {
            while ((line=reader.readLine())!=null) {
                //sb.append(line + ​"​\n​"​);
                sb.append(line+"\n");
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    @Override
    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);
        pozivatelj.onDohvatiDone(rezultat);
    }
}
