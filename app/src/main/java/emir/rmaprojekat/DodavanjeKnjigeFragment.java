package emir.rmaprojekat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

/**
 * Created by user on 12.04.2018..
 */

public class DodavanjeKnjigeFragment extends Fragment implements  View.OnClickListener{
    private Spinner spinerKategorija;
    private ArrayList<String> kategorije;
    private ArrayAdapter<String> adapter;
    private Button ponisti;
    private Button nadjiSliku;
    private Button upisiKnjigu;
    private ArrayList<Knjiga> nizKnjiga;
    private EditText nazivKnjige;
    private EditText imeAutora;
    private ImageView slika;

    private String naslovna;
    private Intent data;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return inflater.inflate(R.layout.fragment_dodavanje_knjige,container,false);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        /*Dohvatiti reference*/
        spinerKategorija=(Spinner) getActivity().findViewById(R.id.sKategorijaKnjige);
        ponisti=(Button) getActivity().findViewById(R.id.dPonisti);
        nadjiSliku=(Button) getActivity().findViewById(R.id.dNadjiSliku);
        upisiKnjigu=(Button) getActivity().findViewById(R.id.dUpisiKnjigu);
        slika= getActivity().findViewById(R.id.naslovnaStr);
        nazivKnjige=(EditText) getActivity().findViewById(R.id.nazivKnjige);
        imeAutora=(EditText) getActivity().findViewById(R.id.imeAutora);
        /*Dohvacanje pod iz Bundle-a*/
        kategorije=getArguments().getStringArrayList("KEY");
        adapter=new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1,kategorije);
        spinerKategorija.setAdapter(adapter);
        /*Postaviti listenere*/
        ponisti.setOnClickListener(this);
        nadjiSliku.setOnClickListener(this);
        upisiKnjigu.setOnClickListener(this);
    }
    @Override
    public void onClick(View view){
        if(view.getId()==R.id.dPonisti){
            ((KategorijeAkt)getActivity()).otvoriListe();
        }
        else if(view.getId()==R.id.dNadjiSliku){
            String a=nazivKnjige.getText().toString();
            if(a.isEmpty()){
                Toast.makeText(getActivity(), "Polje Naziv ne smije biti prazno",
                        Toast.LENGTH_SHORT).show();
                return;
            }
            //provjera da li postoji pod ovim imenom knjiga
            BazaOpenHelper bazaOpenHelper=new BazaOpenHelper(getContext());
            int postoji=bazaOpenHelper.metodaPostoji(BazaOpenHelper.IZBOR_KNJIGA,a);
            bazaOpenHelper.close();
            if(postoji==-1){
                Toast.makeText(getActivity(), "Doslo je do greske",
                        Toast.LENGTH_SHORT).show();
                return;
            }
            if(postoji==1){
                Toast.makeText(getActivity(), "Postoji knjiga pod istim nazivom",
                        Toast.LENGTH_SHORT).show();
                return;
            }
            //
            Intent slikaIntent=new Intent();
            slikaIntent.setType("image/*");
            slikaIntent.setAction(Intent.ACTION_GET_CONTENT);
            if(slikaIntent.resolveActivity(getActivity().getPackageManager())!=null){
                startActivityForResult(Intent.createChooser(slikaIntent,"odaberi sliku"),1);
                //request code samo da moze identificirati kad vrarti rezultat
                //Intent.createChooser forsira chooser ako ima vise aplikacija, mogli smo i bez toga
                //tj startActivityForResult(slikaIntent,1);
                //sad treba dobit URI, od njeg napravit Bitmap i proslijediti imageView-u
            }
        }
        else if(view.getId()==R.id.dUpisiKnjigu){
            String a=imeAutora.getText().toString();
            String nk=nazivKnjige.getText().toString();
            if(a.isEmpty() || nk.isEmpty() || adapter.isEmpty()){
                Toast.makeText(getActivity(), "Nijedno polje ne smije biti prazno",
                        Toast.LENGTH_SHORT).show();
                return;
            }
            String k=spinerKategorija.getSelectedItem().toString();
            //ako je spasena naslovna pod jednim imenom, pa onda naziv promijenjen
            if(naslovna!=null && !naslovna.equals(nk)){
                //1.spasit novi
                FileOutputStream outputStream;
                try {
                    outputStream= getActivity().openFileOutput(nk, Context.MODE_PRIVATE);
                    getBitmapFromUri(data.getData()).compress(Bitmap.CompressFormat.JPEG,90,outputStream);
                    outputStream.close();
                    slika.setImageBitmap(BitmapFactory.decodeStream(getActivity().openFileInput(nk)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //2.obrisat stari
                getActivity().deleteFile(naslovna);
            }
            Knjiga trenutna=new Knjiga(imeAutora.getText().toString(),nk,k);
            //baza
            BazaOpenHelper bazaOpenHelper=new BazaOpenHelper(getContext());
            int postoji=bazaOpenHelper.postojiKnjiga(trenutna);
            if(postoji==1){
                Toast.makeText(getActivity(), "Knjiga vec postoji",
                        Toast.LENGTH_SHORT).show();
                bazaOpenHelper.close();
                return;
            }
            else if(postoji==-1){
                Toast.makeText(getActivity(), "Doslo je do greske",
                        Toast.LENGTH_SHORT).show();
                bazaOpenHelper.close();
                return;
            }
            long idNoveKnjige=bazaOpenHelper.dodajKnjigu(trenutna);//mozda obrisat provjeru iz metode dodajKnjigu
            if(idNoveKnjige==-1){
                Toast.makeText(getActivity(), "Doslo je do greske",
                        Toast.LENGTH_SHORT).show();
                bazaOpenHelper.close();
            }
            else {
                bazaOpenHelper.close();
                //baza
                ((KategorijeAkt) getActivity()).otvoriListe();
                //dodat messagebox il sta vec da obavijesti korisnika
            }
        }
    }
    //kasnije bolje pogledat kako funkcionise ovo pretvaranje Uri u Bitmap
    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getActivity().getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode==1 && resultCode==RESULT_OK){
            //spasava sliku u internu mem aplikacije
            FileOutputStream outputStream;
            String sNazivKnjige=nazivKnjige.getText().toString();
            this.data=data;
            try {
                outputStream= getActivity().openFileOutput(sNazivKnjige, Context.MODE_PRIVATE);
                getBitmapFromUri(data.getData()).compress(Bitmap.CompressFormat.JPEG,90,outputStream);
                outputStream.close();
                slika.setImageBitmap(BitmapFactory.decodeStream(getActivity().openFileInput(sNazivKnjige)));
                naslovna=sNazivKnjige;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
