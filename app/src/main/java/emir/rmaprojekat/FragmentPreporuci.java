package emir.rmaprojekat;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.provider.ContactsContract;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentPreporuci extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>,View.OnClickListener{
    //-------------------atributi-------------------
    private Spinner kontakti;
    private Button posalji,odustani;
    private Knjiga knjiga;

    private TextView naziv,autor,datum,opis,brojStranica;

    private SimpleCursorAdapter mCursorAdapter;
    private final static String[] FROM_COLUMNS = {ContactsContract.Data.DATA1};
    private final static int[] TO_IDS = {android.R.id.text1};
    //od android 6.0-takav je moj emulator
    //Since READ_CONTACTS is a dangerous permission, we need to request the user to grant it at the runtime.
    // Request code for READ_CONTACTS. It can be any number > 0.
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    //PROJECTION
    private static final String[] PROJECTION =
            {
                    ContactsContract.Data._ID,
                    ContactsContract.Data.DISPLAY_NAME_PRIMARY,
                    ContactsContract.Data.DATA1
            };
    //PROJECTION
    //SELECTION
    private static final String SELECTION= ContactsContract.Data.MIMETYPE+" = '"+
            ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE+"'";
    //ContactsContract.Data je generic table i u zavisnosti od mimetype drzi dr vrijednosti na istim poljima
    //SELECTION

    //-------------------konstruktor-------------------
    public FragmentPreporuci() {
        // Required empty public constructor
    }

    //-------------------metode-------------------
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_preporuci, container, false);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        //dohvatiti iz bundle-a//
        knjiga=(Knjiga)getArguments().getSerializable("PREPORUCI");
        //dohvatiti reference//
        kontakti=(Spinner)getActivity().findViewById(R.id.sKontakti);
        posalji=(Button)getActivity().findViewById(R.id.dPosalji);
        odustani=(Button)getActivity().findViewById(R.id.dOdustani);
        posalji.setOnClickListener(this);odustani.setOnClickListener(this);
        naziv=(TextView)getActivity().findViewById(R.id.pNaziv);
        autor=(TextView)getActivity().findViewById(R.id.pAutor);
        datum=(TextView)getActivity().findViewById(R.id.pDatumObjavljivanja);
        opis=(TextView)getActivity().findViewById(R.id.pOpis);
        brojStranica=(TextView)getActivity().findViewById(R.id.pBrojStranica);
        //popuniti podacima
        naziv.setText("Naziv: "+knjiga.getNaziv());
        if(knjiga.getImePrvogAutora()!=null) {
            autor.setText("Prvi pisac: "+knjiga.getImePrvogAutora());
        }
        else{
            autor.setText("Prvi pisac: nema podataka");
        }
        if(knjiga.getOnline()){
            if(knjiga.getDatumObjavljivanja()!=null) {
                datum.setText("Datum: " + knjiga.getDatumObjavljivanja());
            }
            else {datum.setText("Datum: nema podataka");}
            if(knjiga.getOpis()!=null){
                opis.setText("Opis: "+knjiga.getOpis());
            }
            else {opis.setText("Opis: nema podataka");}
            Integer br=knjiga.getBrojStranica();
            if(!br.equals(-1)) {
                String broj = Integer.toString(br);
                brojStranica.setText("Br stranica: " + broj);
            }
            else{
                brojStranica.setText("Br stranica: nema podataka");
            }
        }
        else{
            datum.setText("Datum: nema podataka");
            opis.setText("Opis: nema podataka");
            brojStranica.setText("Br stranica: nema podataka");
        }
        //adapter
        mCursorAdapter=new SimpleCursorAdapter(
                getActivity(),android.R.layout.simple_list_item_1,null,FROM_COLUMNS,TO_IDS, 3
        );
        kontakti.setAdapter(mCursorAdapter);
        //od android 6.0 moramo traziti permisije at runtime
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getActivity().checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        }
        else {
            getLoaderManager().initLoader(0, null, this);
        }

    }
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return  new CursorLoader(
                getActivity(),
                ContactsContract.Data.CONTENT_URI,
                PROJECTION,
                SELECTION,
                null,
                null
                );
    }
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        // Put the result Cursor in the adapter for the ListView
        mCursorAdapter.swapCursor(data);
        //data.moveToFirst();
        //Log.d("poruka",data.getString(1));
    }
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // Delete the reference to the existing Cursor
        mCursorAdapter.swapCursor(null);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                getLoaderManager().initLoader(0, null, this);
            } else {
                Log.d("poruka","trebaju permisije");
            }
        }
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.dPosalji){
            Cursor cursor=mCursorAdapter.getCursor();
            int pos=kontakti.getSelectedItemPosition();
            cursor.moveToPosition(pos);
            String mail=cursor.getString(2);
            String tekst;
            if(knjiga.getImePrvogAutora()!=null) {
                tekst = "Zdravo​ " + cursor.getString(1) + ",\n" +
                        "Pročitaj knjigu " + knjiga.getNaziv() + " od " + knjiga.getImePrvogAutora() + "!";
            }
            else{
                tekst = "Zdravo​ " + cursor.getString(1) + ",\n" +
                        "Pročitaj knjigu " + knjiga.getNaziv() + "!";
            }
            String[] TO = {mail};

            Intent emailIntent=new Intent(Intent.ACTION_SEND);
            emailIntent.setData(Uri.parse("mailto:"));
            emailIntent.setType("text/plain");
            emailIntent.putExtra(Intent.EXTRA_EMAIL,TO);
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Spirala 4");
            emailIntent.putExtra(Intent.EXTRA_TEXT, tekst);
            try {
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                getActivity().finish();
                Log.i("Finished sending email", "");
            }
            catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(getActivity(), "There is no email client installed.",
                Toast.LENGTH_SHORT).show();
            }
        }
        else if(view.getId()==R.id.dOdustani){
            ((KategorijeAkt) getActivity()).otvoriListe();
        }
    }

}
