package emir.rmaprojekat;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.util.Pair;

import java.lang.reflect.Array;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by user on 28.05.2018..
 */

public class BazaOpenHelper extends SQLiteOpenHelper {
    //---------atributi---------
    private Context context;

    public static final int IZBOR_AUTOR=1;
    public static final int IZBOR_KATEGORIJA=2;
    public static final int IZBOR_KNJIGA=3;

    public static final String TABLE_KATEGORIJA="Kategorija";
    public static final String TABLE_KNJIGA="Knjiga";
    public static final String TABLE_AUTOR="Autor";
    public static final String TABLE_AUTORSTVO="Autorstvo";

    public static final String KATEGORIJA_ID="_id";
    public static final String KATEGORIJA_NAZIV="naziv";//text
    public static final String KNJIGA_ID="_id";
    public static final String KNJIGA_NAZIV="naziv";//text...
    public static final String KNJIGA_OPIS="opis";
    public static final String KNJIGA_DATUM="datumObjavljivanja";
    public static final String KNJIGA_STRANICE="brojStranica";//integer
    public static final String KNJIGA_SERVIS="idWebServis";//text
    public static final String KNJIGA_ID_KATEGORIJE="idkategorije";//integer
    public static final String KNJIGA_SLIKA="slika";//text
    public static final String KNJIGA_PREGLEDANA="pregledana";//integer...
    public static final String AUTOR_ID="_id";
    public static final String AUTOR_IME="ime";//text
    public static final String AUTORSTVO_ID="_id";//integer...
    public static final String AUTORSTVO_ID_AUTORA="idautora";
    public static final String AUTORSTVO_ID_KNJIGE="idknjige";

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "mojaBaza.db";

    //SQL upiti za kreiranje tabela, mozemo f-ju naparviti da spaja..al nezz koliko ce ubrzat
    private static final String CREATE_KATEGORIJA="create table "+TABLE_KATEGORIJA+" ("+
            KATEGORIJA_ID+" integer primary key autoincrement, "+
            KATEGORIJA_NAZIV+" text not null);";

    private static final String CREATE_KNJIGA="create table "+TABLE_KNJIGA+" ("+
            KNJIGA_ID+" integer primary key autoincrement, "+
            KNJIGA_NAZIV+" text not null, "+
            KNJIGA_OPIS+" text, "+
            KNJIGA_DATUM+" text, "+
            KNJIGA_STRANICE+" integer, "+
            KNJIGA_SERVIS+" text, "+
            KNJIGA_ID_KATEGORIJE+" integer not null, "+
            KNJIGA_SLIKA+" text, "+
            KNJIGA_PREGLEDANA+" integer not null);";

    private static final String CREATE_AUTOR="create table "+TABLE_AUTOR+" ("+
            AUTOR_ID+" integer primary key autoincrement, "+
            AUTOR_IME+" text not null);";

    private static final String CREATE_AUTORSTVO="create table "+TABLE_AUTORSTVO+" ("+
            AUTORSTVO_ID+" integer primary key autoincrement, "+
            AUTORSTVO_ID_AUTORA+" integer not null, "+
            AUTORSTVO_ID_KNJIGE+" integer not null);";

    //---------konstruktori---------
    public BazaOpenHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
        this.context=context;
    }
    //---------metode---------
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_KATEGORIJA);
        db.execSQL(CREATE_KNJIGA);
        db.execSQL(CREATE_AUTOR);
        db.execSQL(CREATE_AUTORSTVO);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_AUTORSTVO);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_AUTOR);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_KNJIGA);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_KATEGORIJA);
        onCreate(db);
    }
    //---------trazene metode---------
    public long dodajKategoriju(String naziv){
        int postoji=metodaPostoji(IZBOR_KATEGORIJA, naziv);
        if(postoji==1 || postoji==-1) return -1;

        long id;SQLiteDatabase db;
        try {db = getWritableDatabase();} catch(Exception e){e.printStackTrace();return -1;}
        ContentValues values=new ContentValues();
        try {values.put(KATEGORIJA_NAZIV,naziv);}catch (Exception e){e.printStackTrace();db.close();return  -1;}

        id=db.insert(TABLE_KATEGORIJA,null,values);//rowid alias za _id
        Log.d("poruka", String.valueOf(id));
        if(id!=-1) {((KategorijeAkt)context).dodajKategoriju(naziv);}
        db.close();
        return id;
    }

    public long dodajKnjigu(Knjiga knjiga){
        long idKnjige;int postoji=postojiKnjiga(knjiga);
        if(postoji==-1 || postoji==1) return -1;

        long idKategorije=dajIdKategorijeIliAutora(IZBOR_KATEGORIJA,knjiga.getKategorija());
        if(idKategorije==-1) {return  -1;}

        try {
            int pregledana = 0;
            if (knjiga.getKliknuta()) pregledana = 1;
            //dodat u bazu
            SQLiteDatabase db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KNJIGA_NAZIV, knjiga.getNaziv());
            values.put(KNJIGA_OPIS, knjiga.getOpis());
            values.put(KNJIGA_DATUM, knjiga.getDatumObjavljivanja());
            values.put(KNJIGA_STRANICE, knjiga.getBrojStranica());
            values.put(KNJIGA_SERVIS, knjiga.getId());
            values.put(KNJIGA_ID_KATEGORIJE, idKategorije);
            if (knjiga.getSlika() != null) values.put(KNJIGA_SLIKA, knjiga.getSlika().toString());
            else values.put(KNJIGA_SLIKA, "");
            values.put(KNJIGA_PREGLEDANA, pregledana);

            long idNoveKnjige=db.insert(TABLE_KNJIGA, null, values);
            if(idNoveKnjige==-1){
                close();
                return -1;
            }
            db.close();

            idKnjige = dajIdKnjige(knjiga);
            //dodat autore u bazu i listu-ako ne postoje
            ArrayList<Autor> autoriKnjige = knjiga.getAutori();
            if (autoriKnjige != null) {
                for (Autor x : autoriKnjige) {
                    String imeAutora = x.getImeiPrezime();
                    int postojiAutor = metodaPostoji(IZBOR_AUTOR, imeAutora);
                    ;
                    if (postojiAutor == -1) return -1;

                    if (postojiAutor == 0) {
                        //dodati u bazu
                        db = getWritableDatabase();
                        ContentValues values2 = new ContentValues();
                        values2.put(AUTOR_IME, imeAutora);
                        db.insert(TABLE_AUTOR, null, values2);
                        db.close();
                    }
                    long idAutora = dajIdKategorijeIliAutora(IZBOR_AUTOR, imeAutora);
                    if (idAutora == -1) return -1;
                    //dodati u tabelu autorstvo
                    db = getWritableDatabase();
                    ContentValues values2 = new ContentValues();
                    values2.put(AUTORSTVO_ID_AUTORA, idAutora);
                    values2.put(AUTORSTVO_ID_KNJIGE, idKnjige);
                    long idNovogAutorstva=db.insert(TABLE_AUTORSTVO, null, values2);
                    if(idNovogAutorstva==-1){
                        close();
                        return -1;
                    }
                }
            }
            db.close();
            return idKnjige;
        }
        catch (Exception e){e.printStackTrace();close();return -1;}
    }

    public ArrayList<Knjiga> knjigeKategorije(long idKategorije){
        ArrayList<Knjiga> knjigeKategorije=new ArrayList<Knjiga>();
        ArrayList<Integer> ideviKnjiga=new ArrayList<Integer>();

        String[] koloneRezultat={KNJIGA_ID};
        String where=KNJIGA_ID_KATEGORIJE+"="+idKategorije;
        SQLiteDatabase db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_KNJIGA,koloneRezultat,where,null,null,null,null);

        while(cursor.moveToNext()){
            ideviKnjiga.add(cursor.getInt(0));
        }
        cursor.close();db.close();

        for(Integer id:ideviKnjiga){
            Knjiga knjiga=formirajKnjigu(id);
            knjigeKategorije.add(knjiga);
        }
        return knjigeKategorije;
    }

    public ArrayList<Knjiga> knjigeAutora(long idAutora){
        ArrayList<Knjiga> rezultat=new ArrayList<Knjiga>();
        ArrayList<Integer> ideviSvihKnjiga=dajIdeveKnjigaAutora(idAutora);

        for(Integer id:ideviSvihKnjiga){
            Knjiga knjiga=formirajKnjigu(id);
            rezultat.add(knjiga);
        }
        return rezultat;
    }

    //--------------pomocne f--je-------------

    public boolean postaviKliknuta(Knjiga k){
        try{
            long idKnjige=dajIdKnjige(k);
            ContentValues contentValues=new ContentValues();
            contentValues.put(KNJIGA_PREGLEDANA,1);
            String where = KNJIGA_ID + "="+idKnjige;
            SQLiteDatabase db=getWritableDatabase();
            db.update(TABLE_KNJIGA,contentValues,where,null);
            db.close();
        }
        catch (Exception e){e.printStackTrace();close();return false;}
        return true;
    }

    public void ucitajKnjigeUListu(ArrayList<Knjiga> knjige){
        String[] koloneRezultat={KNJIGA_ID};
        SQLiteDatabase db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_KNJIGA,koloneRezultat,null,null,null,null,null);

        while(cursor.moveToNext()){
            Knjiga k=formirajKnjigu(cursor.getInt(0));
            knjige.add(k);
        }
        cursor.close();db.close();
    }

    public boolean ucitajKategorijeUlistu(ArrayList<String> kategorije){
        try {
            String[] koloneRezultat = {KATEGORIJA_NAZIV};
            SQLiteDatabase db = getWritableDatabase();
            Cursor cursor = db.query(TABLE_KATEGORIJA, koloneRezultat, null, null, null, null, null);

            while (cursor.moveToNext()) {
                kategorije.add(cursor.getString(0));
            }
            cursor.close();
            db.close();
        }
        catch (Exception e){e.printStackTrace();close();return false;}
        return true;
    }

    public boolean ucitajAutoreSaIdUlistu(ArrayList<Pair<Integer,Autor>> autoriSaId){
        try {
            String[] koloneRezultat = {AUTOR_ID};
            SQLiteDatabase db = getWritableDatabase();
            Cursor cursor = db.query(TABLE_AUTOR, koloneRezultat, null, null, null, null, null);

            while (cursor.moveToNext()) {
                Autor a = formirajAutora(cursor.getInt(0));
                autoriSaId.add(new Pair<Integer, Autor>(cursor.getInt(0), a));
            }
            cursor.close();
            db.close();
        }
        catch (Exception e){e.printStackTrace();close();return false;}
        return true;
    }

    //-1 error,0 ne postoji,1 postoji;exceptions skupi
    public int postojiKnjiga(Knjiga k){
        ArrayList<Knjiga> knjigeUbazi=new ArrayList<Knjiga>();
        try{ucitajKnjigeUListu(knjigeUbazi);}catch (Exception e) {e.printStackTrace();close();return -1;}

        for(Knjiga x:knjigeUbazi){
            if(k.getOnline() && x.getOnline()) {
                if(k.getId().equals(x.getId())) return 1;
            }
            else {
                if (k.getNaziv().equals(x.getNaziv()))
                    return 1;
            }
        }
        return 0;
    }

    //-1 error,0 ne postoji,1 postoji;exceptions skupi
    public int metodaPostoji(int izbor,String naziv){
        int rezultat = 1;SQLiteDatabase db;
        try{db=getWritableDatabase();}catch (Exception e){e.printStackTrace();return -1;}
        try {
            if (izbor == IZBOR_KATEGORIJA) {
                String[] koloneRezultat = {KATEGORIJA_ID};
                String[] whereArgs={naziv};
                String where = KATEGORIJA_NAZIV + " = ?";
                Cursor cursor = db.query(TABLE_KATEGORIJA, koloneRezultat, where, whereArgs, null, null, null);
                if (cursor.getCount() == 0) rezultat = 0;
                cursor.close();
            } else if (izbor == IZBOR_AUTOR) {
                String[] koloneRezultat = {AUTOR_ID};
                String[] whereArgs={naziv};
                String where = AUTOR_IME + " = ?";
                Cursor cursor = db.query(TABLE_AUTOR, koloneRezultat, where, whereArgs, null, null, null);
                if (cursor.getCount() == 0) rezultat = 0;
                cursor.close();
            }  else if (izbor == IZBOR_KNJIGA) {
                String[] koloneRezultat = {KNJIGA_ID};
                String[] whereArgs={naziv};
                String where = KNJIGA_NAZIV + " = ?";
                Cursor cursor = db.query(TABLE_KNJIGA, koloneRezultat, where, whereArgs, null, null, null);
                if (cursor.getCount() == 0) rezultat = 0;
                cursor.close();
            }
        }
        catch (Exception e){e.printStackTrace();db.close();return -1;}
        db.close();
        return rezultat;
    }
    //-1 error,inace id
    public long dajIdKategorijeIliAutora(int izbor,String naziv){
        long id=-1;SQLiteDatabase db;
        try{db=getWritableDatabase();}catch (Exception e){e.printStackTrace();return -1;}
        try {
            if (izbor == IZBOR_KATEGORIJA) {
                String[] koloneRezultat = {KATEGORIJA_ID};
                String[] whereArgs={naziv};
                String where = KATEGORIJA_NAZIV + " = ?";
                Cursor cursor = db.query(TABLE_KATEGORIJA, koloneRezultat, where, whereArgs, null, null, null);
                cursor.moveToFirst();
                id = cursor.getInt(0);
                cursor.close();
            } else if (izbor == IZBOR_AUTOR) {
                String[] koloneRezultat = {AUTOR_ID};
                String[] whereArgs={naziv};
                String where = AUTOR_IME + " = ?";
                Cursor cursor = db.query(TABLE_AUTOR, koloneRezultat, where, whereArgs, null, null, null);
                cursor.moveToFirst();
                id = cursor.getInt(0);
                cursor.close();
            }
        }
        catch (Exception e){e.printStackTrace();db.close();return -1;}
        db.close();
        return id;
    }

    public int dajIdKnjige(Knjiga k){
        //dodat da poredi i imena pisca?
        SQLiteDatabase db=getWritableDatabase();
        int id;
        String[] koloneRezultat = {KNJIGA_ID};
        String[] whereArgs={k.getNaziv(),k.getId()};
        String where = KNJIGA_NAZIV + " = ? AND "+KNJIGA_SERVIS+" = ?";
        Cursor cursor= db.query(TABLE_KNJIGA, koloneRezultat, where, whereArgs, null, null, null);
        cursor.moveToFirst();
        id = cursor.getInt(0);
        cursor.close();
        return id;
    }

    public String dajNazivKategorije(long idKategorije){
        String[] koloneRezultat={KATEGORIJA_NAZIV};
        String where=KATEGORIJA_ID+"="+idKategorije;
        SQLiteDatabase db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_KATEGORIJA,koloneRezultat,where,null,null,null,null);
        cursor.moveToFirst();

        String nazivKategorije=cursor.getString(0);
        cursor.close();db.close();
        return nazivKategorije;
    }

    public Knjiga formirajKnjigu(long idKnjige){
        //pored podataka iz baze, treba Array<Autor>, te od idKategorije string kategorije
        ArrayList<Autor> autoriKnjige=dajAutoreKnjige(idKnjige);
        String kategorija;Boolean kliknuta=false;

        String[] koloneRezultat={KNJIGA_NAZIV,KNJIGA_OPIS,KNJIGA_DATUM,KNJIGA_STRANICE,KNJIGA_SERVIS,KNJIGA_ID_KATEGORIJE,
                KNJIGA_SLIKA,KNJIGA_PREGLEDANA};
        String where=KNJIGA_ID+"="+idKnjige;
        SQLiteDatabase db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_KNJIGA,koloneRezultat,where,null,null,null,null);

        cursor.moveToFirst();
        kategorija=dajNazivKategorije(cursor.getInt(5));
        URL slika=null;
        try{slika=new URL(cursor.getString(6));}
        catch (Exception e){e.printStackTrace();}
        if(cursor.getInt(7)==1) kliknuta=true;

        Knjiga k=new Knjiga(cursor.getString(4),cursor.getString(0),autoriKnjige,
                cursor.getString(1),cursor.getString(2),slika,cursor.getInt(3));
        if(kliknuta) k.kliknuto();
        k.dopuniOnlineUFull(kategorija);
        //ovdje jos da li je online ili nije
        if(k.getId().equals("")) k.postaviOffline();
        return  k;
    }

    public ArrayList<Autor> dajAutoreKnjige(long idKnjige){
        ArrayList<Autor> rezultat=new ArrayList<Autor>();
        ArrayList<Integer> ideviAutora=new ArrayList<Integer>();

        String[] koloneRezultat={AUTORSTVO_ID_AUTORA};
        String where=AUTORSTVO_ID_KNJIGE+"="+idKnjige;
        SQLiteDatabase db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_AUTORSTVO,koloneRezultat,where,null,null,null,null);

        while(cursor.moveToNext()){
            ideviAutora.add(cursor.getInt(0));
        }
        cursor.close();db.close();

        for(Integer id:ideviAutora){
            Autor autor=formirajAutora(id);
            rezultat.add(autor);
        }
        return rezultat;
    }

    public Autor formirajAutora(long idAutora){
        Autor rezultat;
        //treba nam ime i idWebServisa
        ArrayList<Integer> ideviSvihKnjiga=dajIdeveKnjigaAutora(idAutora);
        ArrayList<String> ideviWebServisa=new ArrayList<String>();
        String imeAutora;

        String[] koloneRezultat={KNJIGA_SERVIS};
        SQLiteDatabase db=getWritableDatabase();
        Cursor cursor;
        for(Integer id:ideviSvihKnjiga){
            String where=KNJIGA_ID+"="+id;
            cursor=db.query(TABLE_KNJIGA,koloneRezultat,where,null,null,null,null);
            cursor.moveToFirst();
            ideviWebServisa.add(cursor.getString(0));
        }
        //sada jos ime
        String[] koloneRezultat2={AUTOR_IME};
        String where=AUTOR_ID+"="+idAutora;
        cursor=db.query(TABLE_AUTOR,koloneRezultat2,where,null,null,null,null);
        cursor.moveToFirst();
        imeAutora=cursor.getString(0);

        cursor.close();db.close();
        rezultat=new Autor(imeAutora,ideviWebServisa);
        return  rezultat;
    }

    public ArrayList<Integer> dajIdeveKnjigaAutora(long idAutora){
        ArrayList<Integer> rezultat=new ArrayList<Integer>();

        String[] koloneRezultat={AUTORSTVO_ID_KNJIGE};
        String where=AUTORSTVO_ID_AUTORA+"="+idAutora;
        SQLiteDatabase db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_AUTORSTVO,koloneRezultat,where,null,null,null,null);

        while(cursor.moveToNext()){
            rezultat.add(cursor.getInt(0));
        }
        db.close();cursor.close();
        return rezultat;
    }

    public Integer dajBrKnjigaAutora(long idAutora){
        String[] koloneRezultat={AUTORSTVO_ID_KNJIGE};
        String where=AUTORSTVO_ID_AUTORA+"="+idAutora;
        SQLiteDatabase db;Integer brKnjiga;
        try{db=getWritableDatabase();}catch(Exception e){e.printStackTrace();return -1;}
        try {
            Cursor cursor=db.query(TABLE_AUTORSTVO,koloneRezultat,where,null,null,null,null);
            brKnjiga=cursor.getCount();
            cursor.close();
        }
        catch (Exception e){db.close();e.printStackTrace();return -1;}
        db.close();
        return brKnjiga;
    }

}
