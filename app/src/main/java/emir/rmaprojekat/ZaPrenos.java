package emir.rmaprojekat;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by user on 30.03.2018..
 */

public class ZaPrenos implements Serializable{
    private ArrayList<Knjiga> knjige;//ovo su reference
    public ZaPrenos(ArrayList<Knjiga> knjige) {
        this.knjige=knjige;
    }
    public ArrayList<Knjiga> dajKnige(){
        return  knjige;
    }
}
