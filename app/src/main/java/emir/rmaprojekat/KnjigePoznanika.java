package emir.rmaprojekat;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.os.ResultReceiver;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by user on 25.05.2018..
 */

public class KnjigePoznanika extends IntentService {
    //atributi
    final public static int STATUS_START=0;
    final public static int STATUS_FINISH=1;
    final public static int STATUS_ERROR=2;
    //konstruktori
    public KnjigePoznanika(String name) {
        super(name);
    }
    public KnjigePoznanika(){
        super(null);
    }
    //metode
    @Override
    public void onCreate(){
        super.onCreate();
    }
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        final ResultReceiver receiver=intent.getParcelableExtra("receiver");
        final String idKorisnika=intent.getStringExtra("idKorisnika");
        ArrayList<Knjiga> rezultat=new ArrayList<Knjiga>();
        Bundle bundle=new Bundle();
        //obavijestiti pocetak
        receiver.send(STATUS_START,Bundle.EMPTY);
        //sad obrada
        String query = null;
        String urlString=null;
        URL url=null;
        URLConnection connection= null;
        HttpURLConnection httpURLConnection=null;
        int responseCode= 0;
        try {
            query = URLEncoder.encode(idKorisnika,"utf-8");
            urlString="https://www.googleapis.com/books/v1/users/"+query+"/bookshelves";
            url=new URL(urlString);
            //sad pozvat servis sa ovim url-om
            connection = url.openConnection();
            httpURLConnection=(HttpURLConnection)connection;
            responseCode = httpURLConnection.getResponseCode();
        } catch (Exception e) {
            e.printStackTrace();
            /* Vrati obavijest da je došlo do izuzetka, e – izuzetak */
            //samo cemo slati kod ovih sto prekidaju servis
            bundle.putString(Intent.EXTRA_TEXT,e.toString());
            receiver.send(STATUS_ERROR,bundle);
            return;
        }
        if(responseCode==HttpURLConnection.HTTP_OK) {
            InputStream in = null;
            String rez=null;
            JSONObject jo = null;//lakse raditi sa JSON
            JSONArray items = null;
            try {
                in = httpURLConnection.getInputStream();
                rez = DohvatiKnjige.CovertStreamToString(in);//da dobijemo string, kojim cemo inicijalizirati JSON objekat
                jo = new JSONObject(rez);
                items = jo.getJSONArray("items");
            } catch (Exception e) {
                e.printStackTrace();
                bundle.putString(Intent.EXTRA_TEXT,e.toString());
                receiver.send(STATUS_ERROR,bundle);
                return;
            }
            Log.d("poruka","pokupio bookshelve");
            for (int i = 0; i < items.length(); i++) {
                JSONObject bookShelf=null;
                String urlStringNovi=null;
                URL urlNovi=null;
                URLConnection connection2= null;
                HttpURLConnection httpURLConnection2=null;
                int responseCode2= 0;
                int volumeCount=0;
                try{
                    bookShelf=items.getJSONObject(i);
                    volumeCount=bookShelf.getInt("volumeCount");
                }
                catch (Exception e){
                    e.printStackTrace();
                    bundle.putString(Intent.EXTRA_TEXT,e.toString());
                    receiver.send(STATUS_ERROR,bundle);
                    return;
                }
                int startIndex=0;
                while(startIndex<volumeCount){
                    try{
                        urlStringNovi=bookShelf.getString("selfLink")+"/volumes?maxResults=40&startIndex="+startIndex;
                        urlNovi=new URL(urlStringNovi);
                        connection2 = urlNovi.openConnection();
                        httpURLConnection2=(HttpURLConnection)connection2;
                        responseCode2 = httpURLConnection2.getResponseCode();
                    }
                    catch(Exception e){
                        e.printStackTrace();
                        bundle.putString(Intent.EXTRA_TEXT,e.toString());
                        receiver.send(STATUS_ERROR,bundle);
                        return;
                    }
                    if(responseCode2==HttpURLConnection.HTTP_OK){
                        InputStream in2 = null;
                        String rez2 = null;
                        JSONObject jo2 = null;
                        JSONArray items2 = null;
                        try{
                            in2 = httpURLConnection2.getInputStream();
                            rez2 = DohvatiKnjige.CovertStreamToString(in2);
                            jo2 = new JSONObject(rez2);
                            items2 = jo2.getJSONArray("items");
                        }
                        catch(Exception e){
                            e.printStackTrace();
                            bundle.putString(Intent.EXTRA_TEXT,e.toString());
                            receiver.send(STATUS_ERROR,bundle);
                            return;
                        }
                        Log.d("poruka","sad ce kroz knjige odr shelva");
                        //sada kroz knjige sve odredenog bookshelva
                        for(int j=0;j<items2.length();j++) {
                            JSONObject knjiga = null;
                            String id = null;
                            JSONObject knjigaInfo = null;
                            String naziv = null;
                            try {
                                knjiga = items2.getJSONObject(j);
                                //id
                                id = knjiga.getString("id");
                                //naziv, autori
                                knjigaInfo = knjiga.getJSONObject("volumeInfo");
                                naziv = knjigaInfo.getString("title");
                            } catch (Exception e) {
                                e.printStackTrace();
                                bundle.putString(Intent.EXTRA_TEXT,e.toString());
                                receiver.send(STATUS_ERROR,bundle);
                                return;
                            }
                            //ove vr moze da nema
                            JSONArray autori = null;
                            //opis,datumObjavljivanja,br Stranica
                            String opis = null;
                            String datumObjavljivanja = null;
                            int brStranica = -1;
                            ArrayList<Autor> authors = new ArrayList<Autor>();
                            try {
                                autori = knjigaInfo.getJSONArray("authors");
                                for (int k = 0; k < autori.length(); k++) {
                                    String autor = null;
                                    try {
                                        autor = autori.getString(k);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    //provjera da li vec postoji
                                    //ako ne postoji, dodati
                                    Autor a1 = new Autor(autor, id);
                                    authors.add(a1);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                opis = knjigaInfo.getString("description");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                datumObjavljivanja = knjigaInfo.getString("publishedDate");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                brStranica = knjigaInfo.getInt("pageCount");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            //slika
                            JSONObject thumb = null;
                            String url1 = null;
                            URL slika = null;
                            try {
                                thumb = knjigaInfo.getJSONObject("imageLinks");
                                url1 = thumb.getString("thumbnail");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                slika = new URL(url1);
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
                            //
                            //treba iznad u try/catch il nest jer neamju svi sve podfatke
                            Knjiga k = new Knjiga(id, naziv, authors, opis, datumObjavljivanja, slika, brStranica);
                            rezultat.add(k);
                            Log.d("poruka", "self: " + i + ", startIndex: "+startIndex+", knjiga: "+j+", " + k.getNaziv());
                        }
                    }
                    startIndex+=40;
                }
            }
        }
        //vracanje podataka
        //bundle.putParcelableArrayList("result",rezultat);-kasnije ovako, prvo treba prepraviti klasuj da mozemo parcelable
        ZaPrenos prenos=new ZaPrenos(rezultat);
        bundle.putSerializable("result",prenos);
        receiver.send(STATUS_FINISH,bundle);
        //kod izuzetaka staviti da salje STATUS_ERROR...
    }
}
