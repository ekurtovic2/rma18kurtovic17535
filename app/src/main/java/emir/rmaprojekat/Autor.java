package emir.rmaprojekat;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by user on 18.05.2018..
 */

public class Autor implements Serializable{
    private String imeiPrezime;
    private ArrayList<String> knjige;//idWebServisa, tj online knjige
    //private ArrayList<Integer> sveKnjigeAutora;//id-evi svih knjiga autora
    //konstruktor
    public Autor(String imeiPrezime){
        this.imeiPrezime = imeiPrezime;
        this.knjige=new ArrayList<String>();
    }
    public Autor(String imeiPrezime, String id) {
        this.imeiPrezime = imeiPrezime;
        this.knjige=new ArrayList<String>();
        this.knjige.add(id);
    }
    public Autor(String imeiPrezime, ArrayList<String> knjige) {
        this.imeiPrezime = imeiPrezime;
        this.knjige=knjige;
    }
    //geteri i seteri
    public String getImeiPrezime() {
        return imeiPrezime;
    }
    public void setImeiPrezime(String imeiPrezime) {
        this.imeiPrezime = imeiPrezime;
    }
    public ArrayList<String> getKnjige() {
        return knjige;
    }
    public void setKnjige(ArrayList<String> knjige) {
        this.knjige = knjige;
    }
    //metode
    public void dodajKnjigu(String id){
        if(!knjige.contains(id)) knjige.add(id);
    }
}
