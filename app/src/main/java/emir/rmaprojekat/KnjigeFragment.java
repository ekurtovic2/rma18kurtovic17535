package emir.rmaprojekat;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by user on 12.04.2018..
 */

public class KnjigeFragment extends Fragment implements View.OnClickListener {
    private String kat;
    private ArrayList<Knjiga> odabraneKnjige;
    private ListView lista;
    private Button povratak;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return inflater.inflate(R.layout.fragment_knjige,container,false);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        povratak=getActivity().findViewById(R.id.dPovratak);
        povratak.setOnClickListener(this);
        /*Dohvacanje pod iz Bundle-a*/
        ZaPrenos primaoc=(ZaPrenos) getArguments().getSerializable("ODABRANE");
        odabraneKnjige=primaoc.dajKnige();//sadryi reference na knjige iy aktivnosti
        //dohvatiti reference
        lista=getActivity().findViewById(R.id.listaKnjiga);
        final MojAdapter adapter=new MojAdapter(getContext(),R.layout.element_liste,odabraneKnjige);
        lista.setAdapter(adapter);
        //ako klikne da ostane plava,mozemo sacuvat u knjizi, mozemo u local storage, a mozemo slat i vracati Bundle savedInstanceState
        //najlakse cuvati u knjizi iako mozda ne treba na ovaj nacin al nije specificirano
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                odabraneKnjige.get(i).kliknuto();
                adapter.notifyDataSetChanged();
                BazaOpenHelper bazaOpenHelper=new BazaOpenHelper(getContext());
                boolean uspio=bazaOpenHelper.postaviKliknuta(odabraneKnjige.get(i));
                if(!uspio){
                    Toast.makeText(getActivity(), "Doslo je do greske",
                            Toast.LENGTH_SHORT).show();
                }
                bazaOpenHelper.close();
            }
        });
    }
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.dPovratak) {
            ((KategorijeAkt)getActivity()).otvoriListe();
        }
    }
}
